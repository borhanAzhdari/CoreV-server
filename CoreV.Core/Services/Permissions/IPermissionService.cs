﻿using CoreV.Domain.DTOs.Permissions;
using CoreV.Domain.Models.Permissions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CoreV.Core.Services.Permissions
{
    public interface IPermissionAppService
    {
        Task<IEnumerable<PermissionDto>> GetGrantedPermissionsAsync(string userNameOrEmail);
        Task<bool> IsUserGrantedToPermissionAsync(string userNameOrEmail, string permissionName);
        void InitializePermissions(List<Permission> permissions);
    }
}
