﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreV.Domain.DTOs.Users
{
    public class CreateOrUpdateUserInput
    {
        public UserDto User { get; set; } = new UserDto();
        public List<Guid> GrantedRoleIds { get; set; } = new List<Guid>();
    }
}
