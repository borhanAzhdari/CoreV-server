﻿using CoreV.Domain.DTOs.Main;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoreV.Domain.DTOs.Users
{
    public class UserDto : EntityDto
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
