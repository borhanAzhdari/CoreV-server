﻿using CoreV.Domain.DTOs.Roles;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoreV.Domain.DTOs.Users
{
    public class GetUserForCreateOrUpdateOutput
    {
        public UserDto User { get; set; } = new UserDto();
        public List<RoleDto> AllRoles { get; set; } = new List<RoleDto>();
        public List<Guid> GrantedRoleIds { get; set; } = new List<Guid>();
    }
}
