﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreV.Domain.DTOs.Account
{
    public class LoginOutput
    {
        public string Token { get; set; }
    }
}
