﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreV.Domain.DTOs.Account
{
    public class ResetPasswordInput
    {
        public string UserNameOrEmail { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
    }
}
