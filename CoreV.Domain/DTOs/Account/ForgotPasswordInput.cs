﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreV.Domain.DTOs.Account
{
    public class ForgotPasswordInput
    {
        public string UserNameOrEmail { get; set; }
    }
}
