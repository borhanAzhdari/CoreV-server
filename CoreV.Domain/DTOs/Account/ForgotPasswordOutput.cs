﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreV.Domain.DTOs.Account
{
    public class ForgotPasswordOutput
    {
        public string ResetToken { get; set; }
    }
}
