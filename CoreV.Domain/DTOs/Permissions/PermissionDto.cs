﻿using CoreV.Domain.DTOs.Main;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoreV.Domain.DTOs.Permissions
{
    public class PermissionDto : EntityDto
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
    }
}
