﻿using CoreV.Domain.DTOs.Main;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoreV.Domain.DTOs.Roles
{
    public class RoleListOutput : PagedListOutput
    {
        public string Name { get; set; }
        public bool IsSystemDefault { get; set; }
    }
}
