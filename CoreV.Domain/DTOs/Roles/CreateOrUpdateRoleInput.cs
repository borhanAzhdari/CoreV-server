﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreV.Domain.DTOs.Roles
{
    public class CreateOrUpdateRoleInput
    {
        public RoleDto Role { get; set; } = new RoleDto();
        public List<Guid> GrantedPermissionIds { get; set; } = new List<Guid>();
    }
}
