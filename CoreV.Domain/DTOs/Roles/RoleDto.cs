﻿using CoreV.Domain.DTOs.Main;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoreV.Domain.DTOs.Roles
{
   public class RoleDto : EntityDto
    {
        public string Name { get; set; }
    }
}
