﻿using CoreV.Domain.DTOs.Permissions;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoreV.Domain.DTOs.Roles
{
    public class GetRoleForCreateOrUpdateOutput
    {
        public RoleDto Role { get; set; } = new RoleDto();

        public List<PermissionDto> AllPermissions { get; set; } = new List<PermissionDto>();

        public List<Guid> GrantedPermissionIds { get; set; } = new List<Guid>();
    }
}
