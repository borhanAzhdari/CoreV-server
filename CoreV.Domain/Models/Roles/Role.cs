﻿using System;
using System.Collections.Generic;
using System.Text;
using CoreV.Domain.Models.Users;
using Microsoft.AspNetCore.Identity;
namespace CoreV.Domain.Models.Roles
{
    public class Role : IdentityRole<Guid>
    {
        public bool IsSystemDefault { get; set; } = false;

        public virtual ICollection<RolePermission> RolePermissions { get; set; } = new List<RolePermission>();

        public virtual ICollection<UserRole> UserRoles { get; set; } = new List<UserRole>();
    }
}
