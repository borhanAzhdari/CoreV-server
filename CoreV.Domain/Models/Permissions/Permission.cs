﻿using CoreV.Domain.Models.Roles;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoreV.Domain.Models.Permissions
{
    public class Permission : BaseEntity
    {
        public string Name { get; set; }

        public string DisplayName { get; set; }

        public virtual ICollection<RolePermission> RolePermissions { get; set; } = new List<RolePermission>();
    }
}
